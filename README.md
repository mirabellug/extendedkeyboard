# Clavier étendu sous Debian et dérivés

Brève présentation de l'utilisation avancée du clavier sous Debian et dérivés (fonctionnalités disponibles sous toutes les distributuions GNU/Linux, mais les spécificités des autres distrobutions ne sont pas traités ici)